"""Module defining entrypoint."""
import asyncio

from mqtt_sensor_feed.hass_hack import disable_block_async_io
from mqtt_sensor_feed.daemon import MqttSensorFeed


def main():
    """Entrypoint function."""
    disable_block_async_io()
    dev = MqttSensorFeed()
    asyncio.run(dev.async_run())


if __name__ == "__main__":
    main()
