"""Common functions."""
import unicodedata

from jinja2 import Template

from mqtt_sensor_feed.const import CLEAN_MAP


def remove_accent(string: str) -> str:
    """Remove accents.

    >>> remove_accent("é")
    'e'
    """
    return "".join(
        [
            c
            for c in unicodedata.normalize("NFKD", string)
            if not unicodedata.combining(c)
        ]
    )


def remove_non_ascii(string: str) -> str:
    """Remove non ascii characters.

    >>> remove_non_ascii("a❄️b")
    'ab'
    """
    return string.encode("ASCII", "ignore").decode()


def clean_string(string):
    """Try to clean email string.

    .. todo:: use a lib for that
    """
    if isinstance(string, bytes):
        string = string.decode()
    for old_str, new_str in CLEAN_MAP:
        string = string.replace(old_str, new_str)
    # TODO make the following lines optionals
    string = remove_accent(string)
    string = remove_non_ascii(string)
    return string


def render_value(template_str, data):
    """Render the value of the sensor."""
    template = Template(template_str)
    return template.render(**data)
