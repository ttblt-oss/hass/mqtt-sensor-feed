"""Hack module to disable Home Assistant block_async_io function."""

# block_async_io

from http.client import HTTPConnection

PUTREQUEST_ORIG = HTTPConnection.putrequest


def disable_block_async_io():
    """Disable Home Assistant block_async_io function."""
    HTTPConnection.putrequest = PUTREQUEST_ORIG
